import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LoginScreen from "./screens/LoginScreen";
import HomeScreen from "./screens/HomeScreen";
import { StatusBar } from "expo-status-bar";
import RegisterScreen from "./screens/RegisterScreen";
import { Provider } from "react-redux";
import { store } from "./context/store";
import { RootSiblingParent } from "react-native-root-siblings";

const Stack = createNativeStackNavigator();

const globalScreenOptions = {
  headerTitleAlign: "center",
  headerStyle: {
    backgroundColor: "#2089dc",
    boxShadow: "0px 0.33px 0px 0px #A6A6AA",
  },
  headerTitleStyle: {
    color: "#fff",
    textAlign: "center",
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "700",
    lineHeight: 22,
    letterSpacing: -0.025,
  },
};

export default function App() {
  return (
    <Provider store={store}>
      <RootSiblingParent>
        <NavigationContainer>
          <StatusBar style='auto' />
          <Stack.Navigator screenOptions={globalScreenOptions}>
            <Stack.Screen
              name='login'
              component={LoginScreen}
              options={{ title: "Login Screen" }}
            />
            <Stack.Screen
              name='register'
              component={RegisterScreen}
              options={{ title: "Register Screen" }}
            />
            <Stack.Screen
              name='home'
              component={HomeScreen}
              options={{ title: "Home" }}
            />
          </Stack.Navigator>
        </NavigationContainer>
      </RootSiblingParent>
    </Provider>
  );
}

const styles = StyleSheet.create({});
