import { produce } from "immer";

import {
  USER_LOGIN_FAIL,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
  USER_REGISTER_FAIL,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_REQUEST,
} from "../constants/userConstants";

export const userLoginReducer = (state = {}, action) => {
  return produce(state, (draft) => {
    switch (action.type) {
      case USER_LOGIN_REQUEST:
        draft.loading = true;
        draft.error = null;
        draft.userInfo = null;
        break;
      case USER_LOGIN_SUCCESS:
        draft.loading = false;
        draft.userInfo = action.payload;
        draft.error = null;
        break;
      case USER_LOGIN_FAIL:
        draft.userInfo = null;
        draft.loading = false;
        draft.error = action.payload;
        break;
      case USER_LOGOUT:
        draft.userInfo = null;
        draft.loading = false;
        draft.error = null;

        break;
      default:
        break;
    }
  });
};

export const userRegisterReducer = (state = {}, action) => {
  switch (action.type) {
    case USER_REGISTER_REQUEST:
      return { ...state, loading: true };
    case USER_REGISTER_SUCCESS:
      return { ...state, loading: false, userInfo: action.payload };
    case USER_REGISTER_FAIL:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};
