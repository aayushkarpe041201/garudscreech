import * as SecureStore from "expo-secure-store";

const SecureDBGateway = {
  save: async function (userInfo) {
    try {
      await SecureStore.setItemAsync("USER_INFO", JSON.stringify(userInfo));
      return true;
    } catch (error) {
      return false;
    }
  },
  load: async function () {
    try {
      const session = await SecureStore.getItemAsync("USER_INFO");
      if (session !== null) {
        let userInfo = JSON.parse(session);
        return userInfo;
      }
      return false;
    } catch (error) {
      return null;
    }
  },
  delete: async function () {
    try {
      await SecureStore.deleteItemAsync("USER_INFO");
      return true;
    } catch (error) {
      return false;
    }
  },
};

export default SecureDBGateway;
