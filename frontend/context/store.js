import { combineReducers } from "redux";
import { configureStore } from "@reduxjs/toolkit";
import { persistReducer } from "redux-persist";
import autoMergeLevel2 from "redux-persist/lib/stateReconciler/autoMergeLevel2";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { userLoginReducer, userRegisterReducer } from "./reducers/userReducers";
import SecureDBGateway from "./LocalStorage";

const lacalUserInfo = SecureDBGateway.load() ? SecureDBGateway.load() : null;

const initialState = {
  userLogin: { userInfo: lacalUserInfo },
};

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  stateReconciler: autoMergeLevel2,
};

const rootReducer = combineReducers({
  userLogin: userLoginReducer,
  userRegister: userRegisterReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

const store = configureStore({
  reducer: persistedReducer,
  preloadedState: initialState,
});

export { store };
