import axios from "axios";
import { baseurl } from "../config";
import {
  USER_LOGIN_FAIL,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
  USER_REGISTER_FAIL,
  USER_REGISTER_REQUEST,
  USER_REGISTER_SUCCESS,
} from "../constants/userConstants";

import SecureDBGateway from "../LocalStorage";

export const login = (email, password) => async (dispatch) => {
  try {
    dispatch({ type: USER_LOGIN_REQUEST });

    // console.log("inside login");

    const customHeaders = {
      "Content-Type": "application/json",
    };

    const config = {
      headers: customHeaders,
    };

    const { data } = await axios.post(
      `${baseurl}/api/users/login`,
      { email, password },
      config
    );

    dispatch({ type: USER_LOGIN_SUCCESS, payload: data });
    await SecureDBGateway.save(data);
  } catch (err) {
    dispatch({
      type: USER_LOGIN_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};

export const logout = (callback) => async (dispatch, getState) => {
  try {
    const deleted = await SecureDBGateway.delete();
    if (deleted) {
      dispatch({ type: USER_LOGOUT });
      callback();
    } else {
      throw new Error("Failed to delete user info from AsyncStorage");
    }
  } catch (error) {
    console.error("Error logging out:", error);
  }
};

export const register = (email, password, name, number) => async (dispatch) => {
  try {
    dispatch({ type: USER_REGISTER_REQUEST });

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const { data } = await axios.post(
      `${baseurl}/api/users`,
      { email, password, name, number },
      config
    );

    dispatch({ type: USER_REGISTER_SUCCESS, payload: data });
    dispatch({ type: USER_LOGIN_SUCCESS, payload: data });

    await SecureDBGateway.save(data);
  } catch (err) {
    dispatch({
      type: USER_REGISTER_FAIL,
      payload:
        err.response && err.response.data.message
          ? err.response.data.message
          : err.message,
    });
  }
};
