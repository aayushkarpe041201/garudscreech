import React, { useState } from "react";
import { StyleSheet, Text, KeyboardAvoidingView, View } from "react-native";
import { Button, Input, Image } from "@rneui/themed";
import { StatusBar } from "expo-status-bar";
import { Platform } from "react-native";

const RegisterScreen = () => {
  const imageUrl =
    "https://imgs.search.brave.com/IWyVa3Q5bPEmoHinP8JD2D4onllkW6sRnnBgkMJKuo8/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9hc3Nl/dHMuc3RpY2twbmcu/Y29tL2ltYWdlcy82/MDAyZjhhYTUxYzJl/YzAwMDQ4YzZjNjgu/cG5n";
  const imageSize = Platform.OS === "web" ? 150 : 200;

  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");

  const handleRegister = () => {
    alert(`Email: ${email}\nPassword: ${password}`);
  };

  return (
    <KeyboardAvoidingView
      behavior='padding'
      style={styles.container}>
      <StatusBar style='light' />
      <Image
        source={{ uri: imageUrl }}
        style={{ height: imageSize, width: imageSize }}
      />
      <Text style={styles.heading}>Please Register!</Text>
      <View style={styles.inputsContainer}>
        <Input
          placeholder='Name'
          autoFocus
          type='text'
          value={name}
          onChangeText={(text) => setName(text)}
        />
        <Input
          placeholder='Email'
          type='email'
          value={email}
          onChangeText={(text) => setEmail(text)}
        />
        <Input
          placeholder='Password'
          secureTextEntry
          type='password'
          value={password}
          onChangeText={(text) => setPassword(text)}
        />
      </View>

      <Button
        title='Register'
        buttonStyle={styles.button}
        onPress={handleRegister}
      />

      <View style={{ height: 100 }} />
    </KeyboardAvoidingView>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
  },
  heading: {
    margin: 20,
    color: "#000",
    textAlign: "center",
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "700",
  },
  button: {
    width: 300,
    marginTop: 10,
  },
  inputsContainer: {
    width: 300,
  },
});
