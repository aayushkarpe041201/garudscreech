import { StyleSheet, Text, View } from "react-native";
import { Button } from "@rneui/themed";
import React from "react";
import { logout } from "../context/actions/userActions";
import { useDispatch, useSelector } from "react-redux";

const HomeScreen = ({ navigation }) => {
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logout(() => navigation.replace("login")));
  };

  const userLogin = useSelector((state) => state.userLogin);
  const { loading, error, userInfo } = userLogin;
  return (
    <View>
      <Text>HomeScreen</Text>

      {userInfo && <Text>{userInfo.name}</Text>}

      <Button
        color='secondary'
        onPress={handleLogout}>
        Logout
      </Button>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({});
