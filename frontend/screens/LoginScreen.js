import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  KeyboardAvoidingView,
  View,
  ActivityIndicator,
} from "react-native";
import { Button, Input, Image } from "@rneui/themed";
import { useDispatch, useSelector } from "react-redux";
import { StatusBar } from "expo-status-bar";
import { Platform } from "react-native";
import { login } from "../context/actions/userActions";

const LoginScreen = ({ navigation }) => {
  const imageUrl =
    "https://imgs.search.brave.com/IWyVa3Q5bPEmoHinP8JD2D4onllkW6sRnnBgkMJKuo8/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9hc3Nl/dHMuc3RpY2twbmcu/Y29tL2ltYWdlcy82/MDAyZjhhYTUxYzJl/YzAwMDQ4YzZjNjgu/cG5n";
  const imageSize = Platform.OS === "web" ? 150 : 200;

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();
  const userLogin = useSelector((state) => state.userLogin);
  const { loading, error, userInfo } = userLogin;

  useEffect(() => {
    if (userInfo?.name) {
      navigation.replace("home");
    }
  }, [userInfo, navigation]);

  const handleLogin = () => {
    dispatch(login(email, password));
  };

  return (
    <KeyboardAvoidingView
      behavior='padding'
      style={styles.container}>
      <StatusBar style='light' />
      <Image
        source={{ uri: imageUrl }}
        style={{ height: imageSize, width: imageSize }}
      />
      <Text style={styles.heading}>Please enter your login credentials!</Text>
      <View style={styles.inputsContainer}>
        <Input
          placeholder='Email'
          autoFocus
          type='email'
          value={email}
          onChangeText={(text) => setEmail(text)}
        />
        <Input
          placeholder='Password'
          secureTextEntry
          type='password'
          value={password}
          onChangeText={(text) => setPassword(text)}
        />
      </View>

      <Button
        title='Login'
        buttonStyle={styles.button}
        onPress={handleLogin}
        loading={loading}
      />
      <Button
        type='outline'
        title='Register'
        buttonStyle={styles.button}
        onPress={() => navigation.navigate("register")}
      />
      <View style={{ height: 100 }} />
    </KeyboardAvoidingView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    padding: 10,
  },
  heading: {
    margin: 20,
    color: "#000",
    textAlign: "center",
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "700",
  },
  button: {
    width: 300,
    marginTop: 10,
  },
  inputsContainer: {
    width: 300,
  },
});
