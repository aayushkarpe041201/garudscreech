import mongoose from "mongoose";

const connectDB = async () => {
  try {
    const conn = await mongoose.connect(process.env.MONGO_URI_local, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
    });
    console.log(
      `MongoDB connected ${conn.connection.host} `.bold.underline.cyan
    );
  } catch (error) {
    console.error(`Error: ${error.message}`.bold.red);
    proccess.exit(1);
  }
};

export default connectDB;
